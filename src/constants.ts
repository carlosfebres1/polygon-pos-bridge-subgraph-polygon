export const RecordStatus__PENDING = "PENDING"
export const RecordStatus__UNKNOWN = "UNKNOWN"
export const RecordStatus__ACTION_REQUIRED = "ACTION_REQUIRED"
export const RecordStatus__SUCCESS = "SUCCESS"
export const RecordStatus__ERROR = "ERROR"

export const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000"
