import {Deposit, Withdraw} from "../generated/schema"
import {log} from "@graphprotocol/graph-ts";
import {getEventId, getOrCreateWallet} from "./helpers";
import {RecordStatus__PENDING, RecordStatus__SUCCESS, ZERO_ADDRESS} from "./constants";
import {Transfer} from "../generated/Contract/ERC20ChildToken";

export function handleTransfer(event: Transfer): void {
    if (event.params.to.toHex() == ZERO_ADDRESS) {
        handleExitedERC20(event)
    } else if (event.params.from.toHex() == ZERO_ADDRESS) {
        handleLockedERC20(event)
    }
}


export function handleLockedERC20(event: Transfer): void {
    log.warning("Tokens Locked: depositor {}", [event.params.to.toHex()])

    getOrCreateWallet(event.params.to.toHex())

    const entity = new Deposit(getEventId(event));
    entity.wallet = event.params.to.toHex()
    entity.status = RecordStatus__SUCCESS
    entity.timestamp = event.block.timestamp
    entity.amount = event.params.value
    entity.txHash = event.transaction.hash
    entity.txBlock = event.block.number
    entity.save()
}

export function handleExitedERC20(event: Transfer): void {
    log.warning("Exited Tokens. exitor: {}", [event.params.from.toHex()])
    getOrCreateWallet(event.params.from.toHex())

    const entity = new Withdraw(getEventId(event))
    entity.wallet = event.params.from.toHex()
    entity.status = RecordStatus__PENDING
    entity.timestamp = event.block.timestamp
    entity.amount = event.params.value
    entity.txHash = event.transaction.hash
    entity.txBlock = event.block.number
    entity.save()
}
